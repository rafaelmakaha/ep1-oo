#include<stdio.h>
#include<stdlib.h>
#define tam 40


typedef struct{
    int xPosi;
    int yPosi;
    int quant;
    int estrutura;
}dados;

void Menu();
void inputUsuario(dados valores, char mundo[tam][tam]);
void iniciaMundo(char mundo[tam][tam], char mundo2[tam][tam]);
void printaMundo(char mundo[tam][tam]);
void rodarJogo(dados valores, char mundo[tam][tam], char mundo2[tam][tam]);
void atualizaMundo(char mundo[tam][tam], char mundo2[tam][tam]);
void aplicaRegra(int x, int y, char mundo[tam][tam], char mundo2[tam][tam]);
void block(int x, int y,char mundo[tam][tam]);
void blinker(int x, int y,char mundo[tam][tam]);
void glider(int x, int y,char mundo[tam][tam]);
void gospelGliderGun(int xPosi, int yPosi, char mundo[tam][tam]);

int main(){

    char mundo[tam][tam], mundo2[tam][tam];
    iniciaMundo(mundo, mundo2);
    Menu();

    return 0;
}


void Menu(char mundo[tam][tam],char mundo2[tam][tam]){
    int opcao;
    dados valores;

    while(opcao){
        system("clear");
        printf("##################\n");
        printf("1.Escolher dados\n");
        printf("2.Iniciar simulação\n");
        printf("Opcao: ");
        scanf("%d",&opcao);
        switch(opcao){
            case 1:
                inputUsuario(valores, mundo);
                break;
            case 2:
                rodarJogo(valores,mundo,mundo2);
                break;
        }
    }
}

void inputUsuario(dados valores, char mundo[tam][tam]){
    int i=0;

    system("clear");
    printf("Quantas estruturas voce deseja adicionar?\n");
    printf("Estruturas: ");
    scanf("%d", &valores.quant);
    while(i < valores.quant){
        printf("Insira a posicao X e Y em que a estrutura sera iniciada: ");
        scanf("%d %d", &valores.xPosi, &valores.yPosi);
        system("clear");
        printf("Qual estrutura voce deseja adicionar?\n");
        printf("1.Block\n");
        printf("2.Blinker\n");
        printf("3.Glider\n");
        printf("4.Gospel Glider Gun\n");
        printf("Estrutura: ");
        scanf("%d", &valores.estrutura);
        switch(valores.estrutura){
            case 1:
                block(valores.xPosi,valores.yPosi,mundo);
                i++;
                break;
            case 2:
                blinker(valores.xPosi,valores.yPosi,mundo);
                i++;
                break;
            case 3:
                glider(valores.xPosi,valores.yPosi,mundo);
                i++;
                break;
            case 4:
                gospelGliderGun(valores.xPosi,valores.yPosi,mundo);
                i++;
                break;
            default:
                printf("Valor inválido\n");
        }
    }
}

void rodarJogo(dados valores, char mundo[tam][tam], char mundo2[tam][tam]){
    int i,j;
    while(1==1){
        printaMundo(mundo);
        getchar();
        getchar();

        for(i=1; i < tam -1; i++){
            for(j=1; j <tam -1; j++){
                aplicaRegra(i,j,mundo,mundo2);
            }
        }

        atualizaMundo(mundo, mundo2);
    }
}

void iniciaMundo(char mundo[tam][tam], char mundo2[tam][tam]){
    int i,j;
    for(i=0; i < tam ; i++){
        for(j=0; j <tam; j++){
            mundo[i][j] = '_';
            mundo2[i][j] = '_';
        }
    }
}
void printaMundo(char mundo[tam][tam]){
    int i,j;
    system("clear");
    for(i=0; i < tam ; i++){
        for(j=0; j <tam; j++){
            printf("%c", mundo[i][j]);
        }
        printf("\n");
    }
}

void atualizaMundo(char mundo[tam][tam], char mundo2[tam][tam]){
    int i,j;
    for(i=0; i < tam ; i++){
        for(j=0; j <tam ; j++){
            mundo[i][j] = mundo2[i][j];
        }
    }
}


void aplicaRegra(int x, int y, char mundo[tam][tam], char mundo2[tam][tam]){
    int i=0, j=0, cont=0;
    int vida=1;

    if(mundo[x][y] == 'O'){
        vida = 1;
    }else{
        vida = 0;
    }
    for(i = x-1; i < x+2 ;i++){
        for(j= y-1; j < y+2 ;j++){
            if(i == x && j == y){
            }else if(mundo[i][j] == 'O'){
                cont++;
            }
        }
    }
    if(cont<2){
        mundo2[x][y] = '_';
    }if(cont>3){
        mundo2[x][y] = '_';
    }if(cont == 3){
        mundo2[x][y] = 'O';
    }else if(cont == 2 && vida){
        mundo2[x][y] = 'O';
    }
}

void block(int x, int y,char mundo[tam][tam]){
    mundo[x][y] = 'O';
    mundo[x][y+1] = 'O';
    mundo[x+1][y] = 'O';
    mundo[x+1][y+1] = 'O';
     
}
void blinker(int x, int y,char mundo[tam][tam]){
    mundo[x][y] = 'O';
    mundo[x][y+1] = 'O';
    mundo[x][y+2] = 'O';
     
}
void glider(int x, int y,char mundo[tam][tam]){
    mundo[x][y+1] = 'O';
    mundo[x+1][y+2] = 'O';
    mundo[x+2][y] = 'O';
    mundo[x+2][y+1] = 'O';
    mundo[x+2][y+2] = 'O';
}

void gospelGliderGun(int xPosi, int yPosi, char mundo[tam][tam]){
    xPosi--; 
    yPosi--;
    

	mundo[xPosi+5][yPosi+11] = 'O';
	mundo[xPosi+6][yPosi+11] = 'O';
	mundo[xPosi+7][yPosi+11] = 'O';
	mundo[xPosi+4][yPosi+12] = 'O';
	mundo[xPosi+8][yPosi+12] = 'O';
	mundo[xPosi+3][yPosi+13] = 'O';
	mundo[xPosi+9][yPosi+13] = 'O';
	mundo[xPosi+3][yPosi+14] = 'O';
	mundo[xPosi+9][yPosi+14] = 'O';
	mundo[xPosi+6][yPosi+15] = 'O';
	mundo[xPosi+5][yPosi+17] = 'O';
	mundo[xPosi+6][yPosi+17] = 'O';
	mundo[xPosi+7][yPosi+17] = 'O';
	mundo[xPosi+4][yPosi+16] = 'O';
	mundo[xPosi+8][yPosi+16] = 'O';
	mundo[xPosi+6][yPosi+18] = 'O';

	mundo[xPosi+3][yPosi+21] = 'O';
	mundo[xPosi+4][yPosi+21] = 'O';
	mundo[xPosi+5][yPosi+21] = 'O';
	mundo[xPosi+3][yPosi+22] = 'O';
	mundo[xPosi+4][yPosi+22] = 'O';
	mundo[xPosi+5][yPosi+22] = 'O';
	mundo[xPosi+2][yPosi+23] = 'O';
	mundo[xPosi+6][yPosi+23] = 'O';
	mundo[xPosi+1][yPosi+25] = 'O';
	mundo[xPosi+2][yPosi+25] = 'O';
	mundo[xPosi+6][yPosi+25] = 'O';
	mundo[xPosi+7][yPosi+25] = 'O';

	block(xPosi, (yPosi + 5), mundo);
	block((xPosi + 35), (yPosi + 3), mundo);

}