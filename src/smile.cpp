#include "smile.hpp"

Smile::Smile(){
    setAltura(7);
    setLargura(5);
    setFigura(getFigura(),getLargura(),getAltura());
}
char** Smile::getFigura(){
    return figura;
}
void Smile::setFigura(char** figura, int largura, int altura){
    int i,j;    
    this->figura = figura = (char**)malloc(altura * sizeof(char*));
    for(i=0; i < altura; i++){    
        this->figura[i] = figura[i] = (char*)malloc(largura * sizeof(char));
    for(j=0; j < largura; j++){        
        this->figura[i][j] = figura[i][j] = '_';
        }
    }
    this->figura[0][4] = 'O';
    this->figura[1][2] = 'O';
    this->figura[1][4] = 'O';
    this->figura[2][0] = 'O';
    this->figura[2][1] = 'O';
    this->figura[3][0] = 'O';
    this->figura[3][1] = 'O';
    this->figura[4][0] = 'O';
    this->figura[4][1] = 'O';
    this->figura[5][2] = 'O';
    this->figura[5][4] = 'O';
    this->figura[6][4] = 'O';
}
int Smile::getLargura(){
    return largura;
}
void Smile::setLargura(int largura){
    this->largura = largura;
}
int Smile::getAltura(){
    return altura;
}
void Smile::setAltura(int altura){
    this->altura = altura;
}
