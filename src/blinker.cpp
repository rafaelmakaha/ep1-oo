#include "blinker.hpp"

Blinker::Blinker(){
    setAltura(1);
    setLargura(3);
    setFigura(getFigura(),getAltura(),getLargura());
}
char** Blinker::getFigura(){
    return figura;
}
void Blinker::setFigura(char** figura, int largura, int altura){
    int i,j;    
    this->figura = figura = (char**)malloc(largura * sizeof(char*));
    for(i=0; i < largura; i++){    
    this->figura[i] = figura[i] = (char*)malloc(altura * sizeof(char));
    for(j=0; j < altura; j++){        
        this->figura[i][j] = figura[i][j] = 'O';
        }
    }
}

int Blinker::getLargura(){
    return largura;
}
void Blinker::setLargura(int largura){
    this->largura = largura;
}
int Blinker::getAltura(){
    return altura;
}
void Blinker::setAltura(int altura){
    this->altura = altura;
}
