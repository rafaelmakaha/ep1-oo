#include "milleniumFalcon.hpp"

MilleniumFalcon::MilleniumFalcon(){
    setAltura(7);
    setLargura(8);
    setFigura(getFigura(),getLargura(),getAltura());
}
char** MilleniumFalcon::getFigura(){
    return figura;
}
void MilleniumFalcon::setFigura(char** figura, int largura, int altura){
    int i,j;    
    this->figura = figura = (char**)malloc(altura * sizeof(char*));
    for(i=0; i < altura; i++){    
        this->figura[i] = figura[i] = (char*)malloc(largura * sizeof(char));
    for(j=0; j < largura; j++){        
        this->figura[i][j] = figura[i][j] = '_';
        }
    } 
    this->figura[0][2] = 'O';
    this->figura[0][3] = 'O';
    this->figura[1][1] = 'O';
    this->figura[1][5] = 'O';
    this->figura[2][0] = 'O';
    this->figura[2][6] = 'O';
    this->figura[3][0] = 'O';
    this->figura[3][4] = 'O';
    this->figura[3][7] = 'O';
    this->figura[3][6] = 'O';
    this->figura[4][0] = 'O';
    this->figura[4][6] = 'O';
    this->figura[5][1] = 'O';
    this->figura[5][5] = 'O';
    this->figura[6][2] = 'O';
    this->figura[6][3] = 'O';
}
int MilleniumFalcon::getLargura(){
    return largura;
}
void MilleniumFalcon::setLargura(int largura){
    this->largura = largura;
}
int MilleniumFalcon::getAltura(){
    return altura;
}
void MilleniumFalcon::setAltura(int altura){
    this->altura = altura;
}
