#include<iostream>
#include<stdlib.h>
#include<unistd.h>

#include"game.hpp"
#include"usuario.hpp"
#include"block.hpp"
#include"blinker.hpp"
#include"glider.hpp"
#include"gosper.hpp"


using namespace std;



int main(){
    int i,j;
    int opcao = 10;
    Game* jogo = new Game();
    Usuario* jogador = new Usuario();
    system("clear");
    jogador->imprimirBoasVindas();
    jogador->Menu();
    while(opcao){
        opcao = jogador->getOpcao();
        switch(opcao){
            case 1:{
                Block* bloco1 = new Block();
                jogo->receberFigura(0,0,jogo->getMundo(),bloco1->getFigura(),bloco1->getLargura(),bloco1->getAltura());
                while(true){
                    system("clear");
                    jogo->imprimirMundo(jogo->getMundo(),jogo->getAltura(),jogo->getLargura());
                    for(i = 0; i < jogo->getAltura(); i++){
                        for(j = 0; j < jogo->getLargura(); j++){
                            jogo->aplicarRegra(i,j,jogo->getMundo(),jogo->getMundo2(),jogo->getAltura(),jogo->getLargura());
                        }
                    }
                    jogo->atualizarMundo(jogo->getMundo(),jogo->getMundo2(),jogo->getAltura(),jogo->getLargura());
                    usleep(300000);
                }
            }
                break;
            case 2:{
                Blinker* blinker1 = new Blinker();
                jogo->receberFigura(1,1,jogo->getMundo(),blinker1->getFigura(),blinker1->getAltura(),blinker1->getLargura());
                while(true){
                    system("clear");
                    jogo->imprimirMundo(jogo->getMundo(),jogo->getAltura(),jogo->getLargura());
                    for(i =0; i < jogo->getAltura(); i++){
                        for(j = 0; j < jogo->getLargura(); j++){
                            jogo->aplicarRegra(i,j,jogo->getMundo(),jogo->getMundo2(),jogo->getAltura(),jogo->getLargura());
                        }
                    }
                    jogo->atualizarMundo(jogo->getMundo(),jogo->getMundo2(),jogo->getAltura(),jogo->getLargura());
                    usleep(300000);
                }
                break;
            }
            case 3:{
                Glider* glider1 = new Glider();
                jogo->receberFigura(0,0,jogo->getMundo(),glider1->getFigura(),glider1->getAltura(),glider1->getLargura());
                while(true){
                    system("clear");
                    jogo->imprimirMundo(jogo->getMundo(),jogo->getAltura(),jogo->getLargura());
                    for(i =0; i < jogo->getAltura(); i++){
                        for(j = 0; j < jogo->getLargura(); j++){
                            jogo->aplicarRegra(i,j,jogo->getMundo(),jogo->getMundo2(),jogo->getAltura(),jogo->getLargura());
                        }
                    }
                    jogo->atualizarMundo(jogo->getMundo(),jogo->getMundo2(),jogo->getAltura(),jogo->getLargura());
                    usleep(300000);
                }
                break;
            }
            case 4:{
                Gosper* gosper = new Gosper();
                jogo->receberFigura(0,0,jogo->getMundo(),gosper->getFigura(),gosper->getAltura(),gosper->getLargura());
                while(true){
                    system("clear");
                    jogo->imprimirMundo(jogo->getMundo(),jogo->getAltura(),jogo->getLargura());
                    for(i =0; i < jogo->getAltura(); i++){
                        for(j = 0; j < jogo->getLargura(); j++){
                            jogo->aplicarRegra(i,j,jogo->getMundo(),jogo->getMundo2(),jogo->getAltura(),jogo->getLargura());
                        }
                    }
                    jogo->atualizarMundo(jogo->getMundo(),jogo->getMundo2(),jogo->getAltura(),jogo->getLargura());
                    usleep(200000);
                }
                break;
            }
            default:
                break;
        }
    }

    return 0;
}
