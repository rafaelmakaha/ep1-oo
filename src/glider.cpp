#include "glider.hpp"

Glider::Glider(){
    setLargura(3);
    setAltura(3);
    setFigura(getFigura(),getAltura(),getLargura());
}
char** Glider::getFigura(){
  return figura;
}
void Glider::setFigura(char** figura, int altura, int largura){
    int i,j;    
    this->figura = figura = (char**)malloc(largura * sizeof(char*));
    for(i=0; i < largura; i++){
      this->figura[i] = figura[i] = (char*)malloc(altura * sizeof(char));
      for(j=0; j < altura; j++){
        this->figura[i][j] = figura[i][j] = '_';
      }
    }
    this->figura[0][1] = 'O';
    this->figura[1][2] = 'O';
    this->figura[2][0] = 'O';
    this->figura[2][1] = 'O';
    this->figura[2][2] = 'O';
}
int Glider::getLargura(){
  return largura;
}
void Glider::setLargura(int largura){
  this->largura = largura;
}
int Glider::getAltura(){
  return altura;
}
void Glider::setAltura(int altura){
  this->altura = altura;
}

