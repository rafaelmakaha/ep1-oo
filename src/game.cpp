#include "game.hpp"
#include<stdlib.h>
#include<iostream>

Game::Game(){
    setAltura(30);
    setLargura(50);
    setMundo(getMundo(),getAltura(),getLargura());
    setMundo2(getMundo2(),getAltura(),getLargura());
}

int Game::getAltura(){
    return altura;
}
void Game::setAltura(int altura){
    this->altura = altura;
}
int Game::getLargura(){
    return largura;
}
void Game::setLargura(int largura){
    this->largura = largura;
}
char** Game::getMundo(){
    return mundo;
}
void Game::setMundo(char** mundo, int altura, int largura){
    int i,j;    
    this->mundo = mundo = (char**)malloc(altura * sizeof(char*));
    for(i=0; i < altura; i++){      
      this->mundo[i] = mundo[i] = (char*)malloc(largura * sizeof(char));
      for(j=0; j < largura; j++){        
        this->mundo[i][j] = mundo[i][j] = '_';
      }
    }
}
char** Game::getMundo2(){
    return mundo2;
}
void Game::setMundo2(char** mundo2, int altura, int largura){
    int i,j;    
    this->mundo2 = mundo2 = (char**)malloc(altura * sizeof(char*));
    for(i=0; i < altura; i++){      
      this->mundo2[i] = mundo2[i] = (char*)malloc(largura * sizeof(char));
      for(j=0; j < largura; j++){        
        this->mundo2[i][j] = mundo2[i][j] = '_';
      }
    }
}
void Game::receberFigura(int xPosi, int yPosi,char** mundo, char** figura, int largura, int altura){
    int i,j;
    for(i = 0; i < largura; i++){
        for(j = 0; j < altura; j++){
            mundo[xPosi + i][yPosi + j] = figura[i][j];
        }
    }
}
void Game::atualizarMundo(char** mundo, char** mundo2,int altura, int largura){
    int i,j;
    for(i=0; i < altura ; i++){
        for(j=0; j <largura ; j++){
            mundo[i][j] = mundo2[i][j];
        }
    }
}
void Game::aplicarRegra(int x, int y,char** mundo, char** mundo2, int altura, int lagrura){
    int i=0, j=0, cont=0;
    bool vida;
    
    if(mundo[x][y] == 'O'){
        vida = true;
    }else{
        vida = false;
    }
    if(x == 0 && y == 0){
        for(i = x; i < x+2 ;i++){
            for(j= y; j < y+2 ;j++){
                if(i == x && j == y){
                }else if(mundo[i][j] == 'O'){
                    cont++;
                }
            }
        }
    }else if(x == altura-1 && y == 0){
        for(i = x-1; i < x+1 ;i++){
            for(j= y; j < y+2 ;j++){
                if(i == x && j == y){
                }else if(mundo[i][j] == 'O'){
                    cont++;
                }
            }
        }
    }else if(x == altura-1 && y == largura-1){
        for(i = x-1; i < x+1 ;i++){
            for(j= y-1; j < y+1 ;j++){
                if(i == x && j == y){
                }else if(mundo[i][j] == 'O'){
                    cont++;
                }
            }
        }
    }else if(x == 0 && y == largura-1){
        for(i = x; i < x+2 ;i++){
            for(j= y-1; j < y+1 ;j++){
                if(i == x && j == y){
                }else if(mundo[i][j] == 'O'){
                    cont++;
                }
            }
        }
    }else if(x == 0){
        for(i = x; i < x+2 ;i++){
            for(j= y-1; j < y+2 ;j++){
                if(i == x && j == y){
                }else if(mundo[i][j] == 'O'){
                    cont++;
                }
            }
        }
    }else if(y == 0){
        for(i = x-1; i < x+2 ;i++){
            for(j= y; j < y+2 ;j++){
                if(i == x && j == y){
                }else if(mundo[i][j] == 'O'){
                    cont++;
                }
            }
        }
    }else if(y == largura-1){
        for(i = x-1; i < x+2 ;i++){
            for(j= y-1; j < y+1 ;j++){
                if(i == x && j == y){
                }else if(mundo[i][j] == 'O'){
                    cont++;
                }
            }
        }    
    }else if(x == altura-1){
        for(i = x-1; i < x+1 ;i++){
            for(j= y-1; j < y+2 ;j++){
                if(i == x && j == y){
                }else if(mundo[i][j] == 'O'){
                    cont++;
                }
            }
        }    
    }else{
        for(i = x-1; i < x+2 ;i++){
            for(j= y-1; j < y+2 ;j++){
                if(i == x && j == y){
                }else if(mundo[i][j] == 'O'){
                    cont++;
                }
            }
        }
    }
    if(cont<2){
        mundo2[x][y] = '_';
    }if(cont>3){
        mundo2[x][y] = '_';
    }if(cont == 3){
        mundo2[x][y] = 'O';
    }else if(cont == 2 && vida){
        mundo2[x][y] = 'O';
    }
}
void Game::imprimirMundo(char** mundo, int altura, int largura){
    int i,j;
    for(i = 0; i < altura; i++){
        for(j = 0; j < largura; j++){
            std::cout << mundo[i][j];
        }
        std::cout << std::endl;
    }
}