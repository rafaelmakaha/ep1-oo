#include "block.hpp"

Block::Block(){
    setAltura(2);
    setLargura(2);
    setFigura(getFigura(),getLargura(),getAltura());
    
}
char** Block::getFigura(){
    return figura;
}
void Block::setFigura(char** figura, int largura, int altura){
    int i,j;
    this->figura = figura = (char**)malloc(largura * sizeof(char*));
    for(i=0; i < largura; i++){
    this->figura[i] = figura[i] = (char*)malloc(altura * sizeof(char));
    for(j=0; j < altura; j++){
        this->figura[i][j] = figura[i][j]= 'O';
        }
    }
}

int Block::getLargura(){
    return largura;
}
void Block::setLargura(int largura){
    this->largura = largura;
}
int Block::getAltura(){
    return altura;
}
void Block::setAltura(int altura){
    this->altura = altura;
}

