# Instruções de uso para o JOGO DA VIDA

## Iniciando...
Caso não tenha clonado o repositório:
```
git clone http://gitlab.com/rafaelmakaha/ep1-oo
cd ep1-oo
make run
```
Caso já tenha clonado o repositório:
```
cd ep1-oo
makerun
```

## Após o Make Run
Aparecerá o seguinte menu:
> 1.Inserir Block
> 2.Inserir Blinker
> 3.Inserir Glider
> 4.Inserir Gosper Glider Gun
> 0.Sair

Para selecionar uma das opções, digite seu número
correspondente e tecle ENTER

## Para sair
Para sair ainda no menu:
    Digite 0

Para sair após executar uma das estruturas utilize o seuinte comando:
    CTRL + z
    ou
    CTRL + c