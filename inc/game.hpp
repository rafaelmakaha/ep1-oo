#ifndef GAME_HPP
#define GAME_HPP

class Game{
private:
    char** mundo;
    char** mundo2;
    int altura;
    int largura;

public:
    Game();
    int getAltura();
    void setAltura(int altura);
    int getLargura();
    void setLargura(int largura);
    char** getMundo();
    void setMundo(char** mundo, int altura, int largura);
    char** getMundo2();
    void setMundo2(char** mundo2, int altura, int largura);
    //Outros
    void receberFigura(int xPosi, int yPosi,char** mundo, char** figura, int largura, int altura);
    void atualizarMundo(char** mundo, char** mundo2, int altura, int largura);
    void aplicarRegra(int xPosi, int yPosi, char** mundo, char** mundo2, int altura, int largura);
    void imprimirMundo(char** mundo, int altura, int largura);


};

#endif