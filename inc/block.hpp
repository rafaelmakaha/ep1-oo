#ifndef BLOCK_HPP
#define BLOCK_HPP

#include "forma.hpp"

class Block: public Forma {
public:
    Block();
    ~Block();
    char** getFigura();
    void setFigura(char** figura, int largura, int altura);
    int getLargura();
    void setLargura(int largura);
    int getAltura();
    void setAltura(int altura);
};

#endif