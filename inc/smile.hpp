#ifndef SMILE_HPP
#define SMILE_HPP

#include "forma.hpp"

class Smile: public Forma{
public:
    Smile();
    ~Smile();
    char** getFigura();
    void setFigura(char** figura, int largura, int altura);
    int getLargura();
    void setLargura(int largura);
    int getAltura();
    void setAltura(int altura);
};

#endif