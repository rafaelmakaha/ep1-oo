#ifndef GLIDER_HPP
#define GLIDER_HPP

#include "forma.hpp"

class Glider : public Forma {
public:
    Glider();
    ~Glider();
    char** getFigura();
    void setFigura(char** figura, int largura, int altura);
    int getLargura();
    void setLargura(int largura);
    int getAltura();
    void setAltura(int altura);
};

#endif