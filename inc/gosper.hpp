#ifndef GOSPER_HPP
#define GOSPER_HPP

#include "forma.hpp"
#include "block.hpp"
#include "milleniumFalcon.hpp"
#include "smile.hpp"

class Gosper : public Forma {
public:
    Gosper();
    ~Gosper();
    char** getFigura();
    void setFigura(char** figura, int largura, int altura);
    void setFigura(char** figura, int largura, int altura, char** block, char** millenium, char** smile);
    int getLargura();
    void setLargura(int largura);
    int getAltura();
    void setAltura(int altura);

};

#endif