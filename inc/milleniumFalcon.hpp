#ifndef MILLENIUMFALCON_HPP
#define MILLENIUMFALCON_HPP

#include "forma.hpp"

class MilleniumFalcon : public Forma {
public:
    MilleniumFalcon();
    ~MilleniumFalcon();
    char** getFigura();
    void setFigura(char** figura, int largura, int altura);
    int getLargura();
    void setLargura(int largura);
    int getAltura();
    void setAltura(int altura);

};

#endif