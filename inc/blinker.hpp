#ifndef BLINKER_HPP
#define BLINKER_HPP

#include "forma.hpp"

class Blinker: public Forma {
public:
    Blinker();
    ~Blinker();
    char** getFigura();
    void setFigura(char** figura, int largura, int altura);
    int getLargura();
    void setLargura(int largura);
    int getAltura();
    void setAltura(int altura);

};

#endif