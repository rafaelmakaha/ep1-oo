#ifndef FORMA_HPP
#define FORMA_HPP

#include<iostream>
#include<string>
#include<stdlib.h>

using namespace std;

class Forma {
  protected:
      int largura;
      int altura;
      char** figura;
    public:
      virtual char** getFigura()=0;
      virtual void setFigura(char** figura, int largura, int altura)=0;
      virtual int getLargura()=0;
      virtual void setLargura(int largura)=0;
      virtual int getAltura()=0;
      virtual void setAltura(int altura)=0;

};

#endif
